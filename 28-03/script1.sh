#!/bin/bash

echo "Test de Conexión a Internet"
echo ""
DOR="sleep 3s"                    #Declaramos la variable DOR 
DOR1="sleep 1s"                   #Declaramos la variable DOR1
$DOR1                             #Ejecutamos la variable

echo "Verificamos realizando ping a los DNS de Google"
echo""
$DOR1

RED="ping -c 3 8.8.8.8"          #Declaramos la variable RED
$RED                             #Ejecutamos la variable
$DOR
echo""
$DOR1
echo $TRUE                      # Hacemos uso del export de la variable TRUE  en el script2.sh              
echo ""
$DOR1
echo "Muchas gracias,Saludos!"
$DOR

#EOF
