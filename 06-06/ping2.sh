#!/bin/bash


#Argumentos
args=( $@ )


#---------------------Varabiables-----------------------------------



EX1='^[1-9]+[0-9]*$' #Expresion regular para validar que el valor ingresado sea un número entero positivo 
EX2='^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$' # Expresion regular para que tome formato ip
EX3='^([a-zA-Z0-9][a-zA-Z0-9-]{0,61}[a-zA-Z0-9]\.)+[a-zA-Z]{2,}$' #Expresion para que valide que el host ingresado no tenga caracteres especiales









#--------------------Funciones-------------------------



ip_valida(){                                #Validamos que la ip ingresada sea valida y que cumpla con la condición que los valores ingresados
					    # sean menores a 255 separado por punto	
         local  ip=$1						
  	local  stat=1
  	if [[ $ip =~ $EX2 ]]; then
        	OIFS=$IFS
        	IFS='.'
        	IP=($ip)
        	IFS=$OIFS
        	[[ ${IP[0]} -le 255 && ${IP[1]} -le 255  && ${IP[2]} -le 255 && ${IP[3]} -le 255 ]]
        	stat=$?					#el resultado del ultimo comando que se guarda en la variable stat 
  	fi
   return $stat     # la funcion se da por concluida y la ejecución continua con el siguente comando despues de la llamada de la función
}

hostname_valido(){ 
	if [[ ${args[-1]} =~ $EX3 ]]; then
		
		return 0
	
	else 
		
		return 1
	fi
}


help_() {                  #Manual de ayuda para el usuario
 
MAN="MANUAL COMANDO PING:
       USO: Script [opcion/opciones] [hostname] o [IP address]

       opciones:

                -C) Para especificar la cantidad de paquetes de solicitud de eco a enviar después
                de lo cual saldrá el ping, use la -C(mayuscula) seguida de la cantidad de paquetes.


                -p) Para establecer el protocolo que puede ver IPV4 o IPV6, use la -p(minuscula) seguida de 4(IPV4) o 6(IPV6)


                -T) Para imprimir la marca de tiempo(tiempo unix + microsegundos) antes de cada linea, use la opción -T

                -b)Para permitir hacer ping a una dirección de broadcast user la opción -b




      "

echo "$MAN"
	
}

array() {

for opcion in "${!args[@]}"; do                 #Invoco el bucle for para que recorra la lista de elementos uno por uno 
       					   	#y en  cada vuelta pase el elemento del valor de la lista en la variable opcion y en cada vuelta
						# va a tener un valor distinto la variable
	case ${args[$opcion]} in


		-C) cantidad=${args[$(($opcion+1))]}  #La variable cantidad va a ser el número actual del elemento más 1
						      #Vuelvo a invocar el array args con el número de indice que viene como opción pero le sumamos 1 	
			
			
	     	       if [[ $cantidad =~ $EX1 ]];then
                          counter="-c $cantidad"           #la variable counter va a tener -c y va a continuar con lo que diga cantidad

                     	else
                                 
                         	echo "Usted debe ingresar un numero entero positivo"
                            
                          	exit 1

                        fi


			;;

		-T) timestamp="-D"

			;;

		-p) proto=${args[$(($opcion+1))]}
                                
			#Valida que el valor ingresado sea 4 0 6

			if [ $proto -eq 4 -o $proto -eq 6 ]; then
                               
			       	p="-$proto"

                        else
                                        
                               echo "Usted puede ingresar unicamente las opciones 4 o 6 para el protocolo de Internet."
                                   
                               exit 1

                        fi


			;;

		-b) b="-b"

			;;



		esac
done

}

# Si no se le pasa un parametro al script va a devolver la ayuda 
if [[ -z "$args" ]]; then


        echo " No pasaste ninguna opción"
	echo " "
        echo "A continuación se mostrata en pantalla el manual del comando PING"
	help_   # Se invoca a la función help_
        exit 1
elif [[ ${#args[@]} -lt 2 ]]; then    #Si los argumentos que son recibidos desde la linea de comandos son menores a 2 
				      #	que devuelva el manual de ayuda 
	
	
	echo "Debe ingresar una opción y argumento para ejecutar comando ping"
 	echo " "
        echo "A continuación se mostrata en pantalla el manual del comando PING"
        help_      # Se invoca a la función help_
        exit 1
fi


if  ip_valida  ${args[-1]}; then   #El if valida si el ultimo argumento es el true de la función ip_valida

        echo "ip valida"

elif hostname_valido ${args[-1]}; then #Si la primera condición no se cumple valida si el ultimo argumento es el true de la función hostname_valido

        echo "Hostname valida"

else
        echo "Por favor ingrese una IP o un hostname valido"

fi

array   #Se invoca a la función array para que recorra el bucle for con las diferentes opciones

ping $b $timestamp $p $counter ${args[-1]} #Se invoca al comando ping y le pasamos las variables declaras 
